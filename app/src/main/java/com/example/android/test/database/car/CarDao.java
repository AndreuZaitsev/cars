package com.example.android.test.database.car;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CarDao {

    @Query("SELECT * FROM cars WHERE id = :carId")
    LiveData<CarEntry> loadCarById(int carId);

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id ORDER BY c.price")
    LiveData<List<CarBrandProducer>> loadAllCarBrandProducer();

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id ORDER BY c.price DESC")
    LiveData<List<CarBrandProducer>> loadAllCarBrandProducerDESC();

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id WHERE p.id = :producerId ORDER BY c.price")
    LiveData<List<CarBrandProducer>> loadCarsByProducer(int producerId);

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id WHERE p.id = :producerId ORDER BY c.price DESC")
    LiveData<List<CarBrandProducer>> loadCarsByProducerDESC(int producerId);

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id WHERE b.id = :brandId ORDER BY c.price")
    LiveData<List<CarBrandProducer>> loadCarsByBrands(int brandId);

    @Transaction
    @Query("SELECT c.* , b.name brandName, p.name producerName, p.id producerId FROM cars c JOIN brands b ON c.brandId = b.id JOIN producers p ON b.producerId = p.id WHERE b.id = :brandId ORDER BY c.price DESC")
    LiveData<List<CarBrandProducer>> loadCarsByBrandsDESC(int brandId);



    @Insert
    void insertCar(CarEntry carEntry);

    @Delete
    void deleteCar(CarEntry carEntry);

    @Update
    void updateCar(CarEntry carEntry);

}
