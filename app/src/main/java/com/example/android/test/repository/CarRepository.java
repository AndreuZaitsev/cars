package com.example.android.test.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.example.android.test.database.CarDb;
import com.example.android.test.database.car.CarBrandProducer;
import com.example.android.test.database.car.CarDao;
import com.example.android.test.database.car.CarEntry;
import com.example.android.test.utils.AppExecutors;

import java.util.List;

public class CarRepository {

    private CarDao carDao;


    public CarRepository(Application application) {

        CarDb mDb = CarDb.getInstance(application);
        carDao = mDb.carDao();

    }

    public LiveData<List<CarBrandProducer>> getCars(boolean descOrder) {
        if (descOrder) {
            return carDao.loadAllCarBrandProducerDESC();
        } else {
            return carDao.loadAllCarBrandProducer();
        }
    }

    public LiveData<List<CarBrandProducer>> getCarsByProducer(int producerId, boolean descOrder) {
        if (descOrder) {
            return carDao.loadCarsByProducerDESC(producerId);
        } else {
            return carDao.loadCarsByProducer(producerId);
        }
    }

    public LiveData<List<CarBrandProducer>> getCarsByBrand(int brandId, boolean descOrder) {
        if (descOrder) {
            return carDao.loadCarsByBrandsDESC(brandId);
        } else {
            return carDao.loadCarsByBrands(brandId);
        }
    }


    public void insert(CarEntry carEntry) {
        AppExecutors.getInstance().diskIO().execute(() -> carDao.insertCar(carEntry));
    }

    public void delete(CarEntry carEntry) {
        AppExecutors.getInstance().diskIO().execute(() -> carDao.deleteCar(carEntry));
    }

    public LiveData<CarEntry> getCarById(int carId) {
        return carDao.loadCarById(carId);
    }

    public void update(CarEntry carEntry) {
        AppExecutors.getInstance().diskIO().execute(() -> carDao.updateCar(carEntry));
    }

}
