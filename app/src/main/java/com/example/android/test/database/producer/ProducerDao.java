package com.example.android.test.database.producer;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Transaction;

import java.util.List;

@Dao
public interface ProducerDao {

    @Transaction
    @Insert
    void insert(List<ProducerEntry> producers);
}
