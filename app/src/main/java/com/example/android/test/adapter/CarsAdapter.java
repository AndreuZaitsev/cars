package com.example.android.test.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.test.R;
import com.example.android.test.database.car.CarBrandProducer;
import com.example.android.test.database.car.CarEntry;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsAdapter extends ListAdapter<CarBrandProducer, CarsAdapter.CarsViewHolder> {

    private List<CarBrandProducer> carFullList;

    private Context mContext;

    public CarsAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.mContext = context;


    }

    private static final DiffUtil.ItemCallback<CarBrandProducer> DIFF_CALLBACK = new DiffUtil.ItemCallback<CarBrandProducer>() {
        @Override
        public boolean areItemsTheSame(CarBrandProducer oldItem, CarBrandProducer newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(CarBrandProducer oldItem, CarBrandProducer newItem) {
            return oldItem.getPrice() == newItem.getPrice() &&
                    oldItem.getType().equals(newItem.getType()) &&
                    oldItem.getBrandId() == newItem.getBrandId() &&
                    oldItem.getUrl().equals(newItem.getUrl()) &&
                    oldItem.getYear() == newItem.getYear();
        }
    };



    @NonNull
    @Override
    public CarsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.car_item, viewGroup, false);
        return new CarsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarsViewHolder holder, int position) {
        CarBrandProducer car = getItem(position);

        if (car.getUrl().equals("default")) {
            Glide.with(mContext).load(R.drawable.default_car).into(holder.ivCar);
        } else {
            Uri imgUri = Uri.parse(car.getUrl());
            Glide.with(mContext).load(imgUri).into(holder.ivCar);
        }

        switch (car.getProducerName()) {
            case "USA":
                Glide.with(mContext).load(R.drawable.ic_usa).into(holder.ivProducer);
                break;
            case "GERMANY":
                Glide.with(mContext).load(R.drawable.ic_germany).into(holder.ivProducer);
                break;
            case "JAPAN":
                Glide.with(mContext).load(R.drawable.ic_japan).into(holder.ivProducer);
                break;

        }

        holder.tvBrand.setText(car.getBrandName());
        holder.tvType.setText(car.getType());
        String year = String.valueOf(car.getYear());
        holder.tvYear.setText(year);
        holder.tvPrice.setText(String.format(Locale.getDefault(), "%,.2f", car.getPrice()));
    }

    public CarBrandProducer getFullCarAt(int position) {
        return getItem(position);
    }

    public CarEntry getCarAt(int position) {
        CarBrandProducer car = getItem(position);
        return new CarEntry(car.getId(), car.getYear(), car.getType(), car.getUrl(), car.getPrice(),car.getBrandId());
    }


    public class CarsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_car) ImageView ivCar;
        @BindView(R.id.iv_producer) ImageView ivProducer;
        @BindView(R.id.tv_brand) TextView tvBrand;
        @BindView(R.id.tv_type) TextView tvType;
        @BindView(R.id.btn_year) TextView tvYear;
        @BindView(R.id.tv_price) TextView tvPrice;

        public CarsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
