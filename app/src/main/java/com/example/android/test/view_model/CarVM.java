package com.example.android.test.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.android.test.database.brand.BrandEntry;
import com.example.android.test.database.car.CarEntry;
import com.example.android.test.repository.BrandRepository;
import com.example.android.test.repository.CarRepository;

import java.util.List;

public class CarVM extends AndroidViewModel {

    private BrandRepository brandRepository;
    private CarRepository carRepository;

    public CarVM(@NonNull Application application) {
        super(application);

        brandRepository = new BrandRepository(application);
        carRepository = new CarRepository(application);
    }

    public LiveData<List<BrandEntry>> getBrands(){
        return brandRepository.getBrands();
    }

    public void insertCar(CarEntry carEntry) {
        carRepository.insert(carEntry);
    }

    public LiveData<CarEntry> getCarById(int carId){
        return carRepository.getCarById(carId);
    }

    public void updateCar(CarEntry carEntry) {
        carRepository.update(carEntry);
    }

}
