package com.example.android.test.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static final String APP_PREFERENCES = "preferences";
    private static final String PREF_DESC_ORDER = "descOrder";
    private static SharedPreferences prefDescOrder;
    private static Boolean descOrder = false;

    public static void setDescOrderPref(Context context, Boolean descOrder){
        prefDescOrder = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefDescOrder.edit();
        editor.putBoolean(PREF_DESC_ORDER, descOrder);
        editor.apply();

    }
    public static Boolean isDescOrder(Context context) {
        prefDescOrder = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        return descOrder = prefDescOrder.getBoolean(PREF_DESC_ORDER, false);
    }


}
