package com.example.android.test.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.android.test.database.brand.BrandDao;
import com.example.android.test.database.brand.BrandEntry;
import com.example.android.test.database.car.CarDao;
import com.example.android.test.database.car.CarEntry;
import com.example.android.test.database.producer.ProducerDao;
import com.example.android.test.database.producer.ProducerEntry;
import com.example.android.test.utils.AppExecutors;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

@Database(entities = {ProducerEntry.class, BrandEntry.class, CarEntry.class}, version = 1, exportSchema = false)
public abstract class CarDb extends RoomDatabase {

    private static final java.lang.Object LOCK = new java.lang.Object();
    private static final String DATABASE_NAME = "carDatabase";
    private static CarDb sInstance;

    public static CarDb getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                Timber.i("Creating new database instance");
                sInstance =
                        Room.databaseBuilder(context.getApplicationContext(), CarDb.class, CarDb.DATABASE_NAME)
                                .addCallback(new RoomDatabase.Callback() {
                                    @Override
                                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                        super.onCreate(db);
                                        AppExecutors.getInstance().diskIO().execute(() -> {
                                            getInstance(context).producerDao().insert(initProducers());
                                            getInstance(context).brandDao().insert(initBrands());
                                        });
                                    }
                                })

                                .build();

            }
        }
        Timber.d("Getting the database instance");
        return sInstance;
    }

    private static List<ProducerEntry> initProducers() {
        List<ProducerEntry> initList = new ArrayList<>();
        initList.add(new ProducerEntry("USA"));
        initList.add(new ProducerEntry("GERMANY"));
        initList.add(new ProducerEntry("JAPAN"));

        return initList;
    }

    private static List<BrandEntry> initBrands() {
        List<BrandEntry> initList = new ArrayList<>();
        initList.add(new BrandEntry("Ford", 1));
        initList.add(new BrandEntry("Chevrolet", 1));
        initList.add(new BrandEntry("Tesla", 1));

        initList.add(new BrandEntry("BMW", 2));
        initList.add(new BrandEntry("Audi", 2));
        initList.add(new BrandEntry("Mercedes", 2));

        initList.add(new BrandEntry("Toyota", 3));
        initList.add(new BrandEntry("Nissan", 3));
        initList.add(new BrandEntry("Honda", 3));

        return initList;
    }

    public abstract ProducerDao producerDao();
    public abstract BrandDao brandDao();
    public abstract CarDao carDao();

}
