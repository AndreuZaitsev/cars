package com.example.android.test.database.producer;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "producers")
public class ProducerEntry {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    public ProducerEntry(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Ignore
    public ProducerEntry(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
