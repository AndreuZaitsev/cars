package com.example.android.test.database.car;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.example.android.test.database.brand.BrandEntry;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "cars", foreignKeys = @ForeignKey(entity = BrandEntry.class, parentColumns = "id", childColumns = "brandId", onDelete = CASCADE))
public class CarEntry {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int year;
    private String type;
    private String url;
    private float price;
    private int brandId;

    public CarEntry(int id, int year, String type, String url, float price, int brandId) {
        this.id = id;
        this.year = year;
        this.type = type;
        this.url = url;
        this.price = price;
        this.brandId = brandId;
    }

    @Ignore
    public CarEntry(int year, String type, String url, float price, int brandId) {
        this.year = year;
        this.type = type;
        this.url = url;
        this.price = price;
        this.brandId = brandId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}
