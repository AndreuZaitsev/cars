package com.example.android.test.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.test.database.brand.BrandEntry;

import java.util.List;

public class BrandSpinnerAdapter extends ArrayAdapter<BrandEntry> {

    private List<BrandEntry> brandsList;

    public BrandSpinnerAdapter(Context context, int textViewResourceId, List<BrandEntry> brandsList) {
        super(context, textViewResourceId, brandsList);
        this.brandsList = brandsList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.WHITE);
        label.setElegantTextHeight(true);


        label.setText(brandsList.get(position).getName());

        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setElegantTextHeight(true);

        label.setText(brandsList.get(position).getName());

        return label;
    }


    @Override
    public int getCount(){
        if (brandsList == null) {
            return 0;
        }
        return brandsList.size();
    }

    @Override
    public BrandEntry getItem(int position){
        return brandsList.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


}
