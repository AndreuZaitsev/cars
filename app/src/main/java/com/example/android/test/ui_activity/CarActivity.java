package com.example.android.test.ui_activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android.test.R;
import com.example.android.test.adapter.BrandSpinnerAdapter;
import com.example.android.test.database.brand.BrandEntry;
import com.example.android.test.database.car.CarEntry;
import com.example.android.test.view_model.CarVM;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarActivity extends AppCompatActivity {
    @BindView(R.id.spinner_brand) Spinner brandSpinner;
    @BindView(R.id.spinner_class) Spinner classSpinner;
    @BindView(R.id.iv_car) ImageView ivCar;
    @BindView(R.id.tv_price) EditText etPrice;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.edit_toolbar) Toolbar mToolbar;
    @BindView(R.id.fab_save_car) FloatingActionButton fabSave;
    @BindView(R.id.btn_year) Button btnYear;

    private static final int SELECT_IMG_REQUEST = 100;

    public static final String EXTRA_CAR_ID = "extraCarId";
    // Extra for the car ID to be received after rotation
    public static final String INSTANCE_CAR_ID = "instanceCarId";

    public static final String INSTANCE_YEAR = "instanceYear";
    public static final String INSTANCE_URL = "instanceUrl";

    public static final int DEFAULT_CAR_ID = -1;

    private CarVM viewModel;
    // for update car
    private int mCarId = DEFAULT_CAR_ID;

    // for new CarEntry
    private int mYear;
    private boolean mYearIsSelected = false;

    private String mUrl;
    private BrandEntry brandEntry;
    private String carClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        ButterKnife.bind(this);

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_CAR_ID)) {
            mCarId = savedInstanceState.getInt(INSTANCE_CAR_ID, DEFAULT_CAR_ID);
            mUrl = savedInstanceState.getString(INSTANCE_URL);
            Glide.with(this).load(mUrl).into(ivCar);

            mYear = savedInstanceState.getInt(INSTANCE_YEAR);
            mYearIsSelected = true;
            btnYear.setText(String.valueOf(mYear));

        } else {
            Glide.with(this).load(R.drawable.default_car).into(ivCar);

        }

        setToolbar();

        setupTypeSpinner();

        setupViewModel();
    }

    private void updateUI() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(EXTRA_CAR_ID)) {
            tvTitle.setText(getString(R.string.update));
            if (mCarId == DEFAULT_CAR_ID) {
                mCarId = intent.getIntExtra(EXTRA_CAR_ID, DEFAULT_CAR_ID);
                viewModel.getCarById(mCarId).observe(this, car -> {

                    int classSpinnerId = 0;
                    switch (car.getType()) {
                        case "Business":
                            classSpinnerId = 0;
                            break;
                        case "Sport":
                            classSpinnerId = 1;
                            break;
                        case "Budgetary":
                            classSpinnerId = 2;
                            break;
                    }
                    classSpinner.setSelection(classSpinnerId, true);

                    mUrl = car.getUrl();
                    if (!mUrl.equals("default"))
                        Glide.with(this).load(mUrl).into(ivCar);

                    fabSave.setImageResource(R.drawable.ic_check_white_24dp);

                    etPrice.setText(String.valueOf(car.getPrice()));

                    mYearIsSelected = true;
                    btnYear.setText(String.valueOf(car.getYear()));
                    mYear = car.getYear();

                    brandSpinner.setSelection(car.getBrandId() - 1, true);

                });

            }
        }
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private void setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(CarVM.class);

        viewModel.getBrands().observe(this, brands -> {
            setupBrandSpinner(brands);
            updateUI();
        });

    }

    private void setupBrandSpinner(List<BrandEntry> spinnerBrands) {
        BrandSpinnerAdapter brandAdapter = new BrandSpinnerAdapter(this, android.R.layout.simple_spinner_item, spinnerBrands);

        brandSpinner.setAdapter(brandAdapter);
        brandAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                brandEntry = brandAdapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupTypeSpinner() {
        ArrayAdapter classAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_class_options, android.R.layout.simple_spinner_item);

        classSpinner.setAdapter(classAdapter);
        classAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                carClass = (String) adapterView.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.btn_year)
    public void selectYear(Button btn) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT,
                (view, year1, month1, dayOfMonth) -> {
                    btn.setText(String.valueOf(year1));
                    mYear = year1;
                    mYearIsSelected = true;
                }, year, month, day) {

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                getDatePicker().findViewById(getResources().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
                getDatePicker().findViewById(getResources().getIdentifier("month", "id", "android")).setVisibility(View.GONE);

            }
        };

        dialog.setTitle(getString(R.string.select_a_year));
        dialog.show();
    }

    @OnClick(R.id.btn_load_image)
    public void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMG_REQUEST);
    }

    @OnClick(R.id.fab_save_car)
    public void saveCar(View view) {
        try {

            if (!mYearIsSelected) throw new Exception("Select year");

            float price = Float.parseFloat(etPrice.getText().toString());
            if (price == 0) throw new Exception("Price should be greater than zero");

            if (mUrl == null) mUrl = "default";

            CarEntry carEntry = new CarEntry(mYear, carClass, mUrl, price, brandEntry.getId());

            if (mCarId == DEFAULT_CAR_ID) {
                viewModel.insertCar(carEntry);
                finish();
            } else {
                carEntry.setId(mCarId);
                viewModel.updateCar(carEntry);
                finish();
            }


        } catch (NumberFormatException e) {
            Toast.makeText(this, "Wrong value", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SELECT_IMG_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri selectedImage = data.getData();
            mUrl = String.valueOf(selectedImage);

            Glide.with(this).load(selectedImage).into(ivCar);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(INSTANCE_CAR_ID, mCarId);
        outState.putInt(INSTANCE_YEAR, mYear);
        outState.putString(INSTANCE_URL, mUrl);

        super.onSaveInstanceState(outState);
    }
}
