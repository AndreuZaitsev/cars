package com.example.android.test.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.android.test.database.brand.BrandEntry;
import com.example.android.test.database.car.CarBrandProducer;
import com.example.android.test.database.car.CarEntry;
import com.example.android.test.repository.BrandRepository;
import com.example.android.test.repository.CarRepository;

import java.util.List;

public class MainVM extends AndroidViewModel {

    private CarRepository carRepository;

    private BrandRepository brandRepository;

    public MainVM(@NonNull Application application) {
        super(application);

        carRepository = new CarRepository(application);

        brandRepository = new BrandRepository(application);
    }

    public LiveData<List<CarBrandProducer>> getCars(boolean descOrder){
        return carRepository.getCars(descOrder);
    }

    public LiveData<List<CarBrandProducer>> getCarsByProducer(int producerId, boolean descOrder){
        return carRepository.getCarsByProducer(producerId,descOrder);
    }

    public LiveData<List<CarBrandProducer>> getCarsByBrand(int brandId, boolean descOrder){
        return carRepository.getCarsByBrand(brandId,descOrder);
    }


    public void deleteCar(CarEntry carEntry){
        carRepository.delete(carEntry);
    }

    public LiveData<List<BrandEntry>> getBrands(){
        return brandRepository.getBrands();
    }


}
