package com.example.android.test.database.brand;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.example.android.test.database.producer.ProducerEntry;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "brands",
        foreignKeys = @ForeignKey(entity = ProducerEntry.class, parentColumns = "id", childColumns = "producerId", onDelete = CASCADE))
public class BrandEntry {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private int producerId;

    public BrandEntry(int id, String name, int producerId) {
        this.id = id;
        this.name = name;
        this.producerId = producerId;
    }

    @Ignore
    public BrandEntry(String name, int producerId) {
        this.name = name;
        this.producerId = producerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProducerId() {
        return producerId;
    }

    public void setProducerId(int producerId) {
        this.producerId = producerId;
    }
}
