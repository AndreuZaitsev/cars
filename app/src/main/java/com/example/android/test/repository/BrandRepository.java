package com.example.android.test.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.example.android.test.database.CarDb;
import com.example.android.test.database.brand.BrandDao;
import com.example.android.test.database.brand.BrandEntry;

import java.util.List;

public class BrandRepository {
    private BrandDao brandDao;

    private LiveData<List<BrandEntry>> brands;

    public BrandRepository(Application application) {

        CarDb mDb = CarDb.getInstance(application);
        brandDao = mDb.brandDao();

        brands = brandDao.loadBrands();
    }

    public LiveData<List<BrandEntry>> getBrands() {
        return brands;
    }
}
