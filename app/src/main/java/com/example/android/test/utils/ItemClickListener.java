package com.example.android.test.utils;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view, int position);

    void onItemLongClickListener(View view, int position);

}
