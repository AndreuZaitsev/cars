package com.example.android.test.ui_activity;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.test.R;
import com.example.android.test.adapter.CarsAdapter;
import com.example.android.test.database.brand.BrandEntry;
import com.example.android.test.database.car.CarBrandProducer;
import com.example.android.test.utils.ItemClickListener;
import com.example.android.test.utils.Preferences;
import com.example.android.test.utils.RecyclerItemTouchHelper;
import com.example.android.test.view_model.MainVM;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.example.android.test.ui_activity.CarActivity.EXTRA_CAR_ID;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_PERMISSION_READ_CONTACTS = 777;
    private static final String INSTANCE_PRODUCER_ID = "instanceProducerId";
    private static final String INSTANCE_BRAND_ID = "instanceBrandId";
    private static final String INSTANCE_MENU_ITEM_TITLE = "instanceMenuItemTitle";

    private static final String FILTER_PRODUCER = "filterProducer";
    private static final String FILTER_BRAND = "filterBrand";

    private static final int DEFAULT_NO_ID = -1;

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.car_recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.viewstub) ConstraintLayout viewStub;
    @BindView(R.id.fab_add_car) FloatingActionButton fabAddCar;
    @BindView(R.id.card_filter) CardView cardFilter;
    @BindView(R.id.tvFilter) TextView tvFilter;


    private CarsAdapter mAdapter;

    private MainVM viewModel;

    private List<Integer> brandMenuIds;

    private int selecetedProducerId = DEFAULT_NO_ID;
    private int selecetedBrandId = DEFAULT_NO_ID;
    private String menuItemTitle;

    private boolean descOrder;

    private Bundle mSavedInstanceState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Timber.plant(new Timber.DebugTree());

        setSupportActionBar(mToolbar);

        setupRecyclerView();

        viewModel = ViewModelProviders.of(this).get(MainVM.class);

        int permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION_READ_CONTACTS);
            mSavedInstanceState = savedInstanceState;
        } else {
            setupViewModel(savedInstanceState);
        }

    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new CarsAdapter(this);

        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemTouchHelper(this, mRecyclerView, new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                updateCar(mAdapter.getCarAt(position).getId());
            }

            @Override
            public void onItemLongClickListener(View view, int position) {
                // handle long click
            }
        }));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    fabAddCar.show();
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fabAddCar.isShown())
                    fabAddCar.hide();
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                deleteCar(viewHolder);
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                RecyclerItemTouchHelper.drawDeletionBackground(getApplicationContext(), c, viewHolder, dX);
            }
        }).attachToRecyclerView(mRecyclerView);

    }

    private void setupViewModel(Bundle bundle) {
        descOrder = Preferences.isDescOrder(this);

        if (bundle != null && bundle.containsKey(INSTANCE_MENU_ITEM_TITLE)) {
            menuItemTitle = bundle.getString(INSTANCE_MENU_ITEM_TITLE);
        }

        if (bundle != null && bundle.containsKey(INSTANCE_PRODUCER_ID)) {
            int producerId = bundle.getInt(INSTANCE_PRODUCER_ID);
            filterCars(producerId, FILTER_PRODUCER);

        } else if (bundle != null && bundle.containsKey(INSTANCE_BRAND_ID)) {
            int brandId = bundle.getInt(INSTANCE_BRAND_ID);
            filterCars(brandId, FILTER_BRAND);

        } else if (menuItemTitle != null) {
            if (selecetedBrandId != DEFAULT_NO_ID) {
                filterCars(selecetedBrandId, FILTER_BRAND);
            } else if (selecetedProducerId != DEFAULT_NO_ID){
                filterCars(selecetedProducerId, FILTER_PRODUCER);
            }
        }
        else {
            viewModel.getCars(descOrder).observe(this, this::updateRecyclerView);
        }
    }

    private void updateRecyclerView(List<CarBrandProducer> cars) {
        mAdapter.submitList(cars);

        if (cars != null && cars.isEmpty()) {
            viewStub.setVisibility(View.VISIBLE);
        } else {
            viewStub.setVisibility(View.GONE);
        }
    }

    private void deleteCar(RecyclerView.ViewHolder viewHolder) {
        viewModel.deleteCar(mAdapter.getCarAt(viewHolder.getAdapterPosition()));
    }

    private void updateCar(int position) {
        Intent intent = new Intent(MainActivity.this, CarActivity.class);
        intent.putExtra(EXTRA_CAR_ID, position);
        startActivity(intent);
    }

    private void filterCars(int id, String filterConst) {
        if (filterConst.equals(FILTER_PRODUCER)) {
            selecetedProducerId = id;
            selecetedBrandId = DEFAULT_NO_ID;
            viewModel.getCarsByProducer(selecetedProducerId, descOrder).observe(this, this::updateRecyclerView);
        } else if (filterConst.equals(FILTER_BRAND)) {
            selecetedBrandId = id;
            selecetedProducerId = -DEFAULT_NO_ID;
            viewModel.getCarsByBrand(selecetedBrandId, descOrder).observe(this, this::updateRecyclerView);
        }

        tvFilter.setText(String.format(getString(R.string.filter_by), menuItemTitle));
        cardFilter.setVisibility(View.VISIBLE);

    }


    @OnClick(R.id.fab_add_car)
    public void addCar() {
        Intent intent = new Intent(MainActivity.this, CarActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.card_filter)
    public void closeFilter(){
        selecetedBrandId = DEFAULT_NO_ID;
        selecetedProducerId = DEFAULT_NO_ID;
        menuItemTitle = null;
        viewModel.getCars(descOrder).observe(this, this::updateRecyclerView);
        cardFilter.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_READ_CONTACTS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission allowed
                    setupViewModel(mSavedInstanceState);

                } else {
                    // permission denied
                    Toast.makeText(this, "You should grant permissions!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_filter, menu);

        menu.getItem(0).getSubMenu().getItem(2).setChecked(descOrder);

        SubMenu brandMenu = menu.getItem(0).getSubMenu().getItem(1).getSubMenu();
        viewModel.getBrands().observe(this, brands -> {
            if (brands != null) {
                brandMenuIds = new ArrayList<>();
                for (BrandEntry brand : brands) {
                    brandMenuIds.add(brand.getId());
                    brandMenu.add(0, brand.getId(), 0, brand.getName());
                }
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.item1:
                menuItemTitle = item.getTitle().toString();
                filterCars(1, FILTER_PRODUCER);
                return true;

            case R.id.item2:
                menuItemTitle = item.getTitle().toString();
                filterCars(2, FILTER_PRODUCER);
                return true;

            case R.id.item3:
                menuItemTitle = item.getTitle().toString();
                filterCars(3, FILTER_PRODUCER);
                return true;

            case R.id.price_order:
                if(item.isChecked()){
                    // If item already checked then unchecked it
                    item.setChecked(false);
                    Preferences.setDescOrderPref(this, false);
                }else{
                    // If item is unchecked then checked it
                    item.setChecked(true);
                    Preferences.setDescOrderPref(this, true);
                }
                setupViewModel(mSavedInstanceState);
                return true;

            default:

                for (int id : brandMenuIds) {
                    if (item.getItemId() == id) {
                        menuItemTitle = item.getTitle().toString();
                        filterCars(id, FILTER_BRAND);
                        return true;
                    }
                }
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (selecetedProducerId != DEFAULT_NO_ID)
            outState.putInt(INSTANCE_PRODUCER_ID, selecetedProducerId);
        if (selecetedBrandId != DEFAULT_NO_ID)
            outState.putInt(INSTANCE_BRAND_ID, selecetedBrandId);
        if (menuItemTitle != null){
            outState.putString(INSTANCE_MENU_ITEM_TITLE, menuItemTitle);
        }
        super.onSaveInstanceState(outState);
    }
}
