package com.example.android.test.database.car;

public class CarBrandProducer {

    private int id;
    private int year;
    private String type;
    private String url;
    private float price;
    private int brandId;
    private String brandName;
    private int producerId;
    private String producerName;


    public CarBrandProducer(int id, int year, String type, String url, float price, int brandId, String brandName, int producerId, String producerName) {
        this.id = id;
        this.year = year;
        this.type = type;
        this.url = url;
        this.price = price;
        this.brandId = brandId;
        this.brandName = brandName;
        this.producerId = producerId;
        this.producerName = producerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getProducerId() {
        return producerId;
    }

    public void setProducerId(int producerId) {
        this.producerId = producerId;
    }

    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }
}

